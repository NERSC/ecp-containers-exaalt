# EXAALT on Perlutter

Directions and materials for running Rahul's EXAALT LAMMPS Snapc benchmark
on Perlmutter in a Shifter container.

## How to build

You can build this image on any x86 machine via `docker build` or
`podman build`. 

## OR How to pull pre-built image

`shifterimg pull docker.io/stephey/exaalt:perlmutter`

## How to run

There are two scripts- the first is the outer wrapper called `submit.sh`.
This is where you set the problem size and node size.

The second is the `run_shifter.sub` script. You may need to adjust the SLURM
job defaults to your NERSC project, desired queue, etc. 

To run:

```
./submit.sh
```

## Resources

[How to use Shifter](https://docs.nersc.gov/development/shifter/how-to-use/)


