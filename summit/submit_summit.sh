#!/bin/bash

# 28 - 1 billion
# 75 - 20 billion
#for nrep in 2 3 4 6 8 13 16 28 75 #32 45 75
#i#for nrep in 28

nodes=2
nsize=4
export benchmark='summit'
echo "requested benchmark type $benchmark"

for nnodes in $nodes
do
    for nrep in $nsize
    do
        sed 's/_NTASKS_/'$(( $nnodes*4 ))'/g' run_summit.sub> temp.sub.$nrep.$nnodes
        sed -i 's/_NREP_/'$(( $nrep ))'/g' temp.sub.$nrep.$nnodes
        bsub temp.sub.$nrep.$nnodes
    done
done
echo "submitted $benchmark"

