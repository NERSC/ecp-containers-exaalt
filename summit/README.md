# EXAALT on Summit

Directions and materials for running Rahul's EXAALT LAMMPS Snapc benchmark
on Summit in a container.

## How to build from source

I'll detail how to actually build the image from source, but if you
don't want to do this, you can skip this section.

First we'll build using the Dockerfile in this directory. It uses
the OLCF-provided base image for CUDA-aware MPI, although note, their
base image still does not actually have all the Spectrum MPI in it, 
which I initially found confusing. Note that this is an Centos
based image. We install the requirements for EXAALT LAMMPS on
top.

```
module purge
module load DefApps
module load gcc/9.1.0

podman build --build-arg mpi_root=$MPI_ROOT -v $MPI_ROOT:$MPI_ROOT -f Dockerfile -t exaalt:summit
podman save -o exaalt-summit.tar localhost/exaalt:summit
```

## OR How to pull pre-built image

This build will take a long time. Alternatively, you can pull the previously
built image from dockerhub via

```
podman pull docker.io/stephey/exaalt:summit
podman save -o exaalt-summit.tar docker.io/stephey/exaalt:summit 
```

The second step prepares for conversion to singularity.

## How to run

Once you have either built or pulled the image, we have to convert to Singuarlity since
that's the OLCF runtime solution. To avoid running out of resources during this process,
I just did it in a job using the `build_singularity.sub` script in this repo.

```
bsub build_singularity.sub
```

After you have converted your Docker image to a singularity image, you're ready to run.

There are two scripts- the first is the outer wrapper called `submit_summit.sh`. This
is where you can set the problem size and the node size.

The second is the `run_summit.sub` script. Note that you'll also need to modify this
script to change the number of nodes in the job (since I couldn't find an 
environment variable to access the job size.

Note you will also need to modify things like the singularity file name and the 
path to your benchmark directory. 

To run:

```
./submit_summit.sh
```

A comment on the job structure: getting the resource set configuration 
set correctly is key in getting this to run. It seems that each node should have its
own resource set so that EXAALT/LAMMPS can control the assignement of GPUs. For example:

```
jsrun -a 6 -c 6 -g 6 -d packed -l gpu-cpu
```

is key.

## Resources
OLCF base images: https://code.ornl.gov/76a/olcfbaseimages

Building CUDA-aware images on Summit via podman
https://docs.olcf.ornl.gov/software/containers_on_summit.html#running-a-cuda-aware-mpi-program-with-the-olcf-mpi-base-image


