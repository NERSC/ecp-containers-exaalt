# ecp-containers-exaalt

## Overview

One of the ECP containers project milestones was to demonstrate a containerized
ECP app running on several large systems. We have chosen LAMMPS/EXAALT.

We would gratefully like to acknowledge [Rahul Gayatri](https://www.nersc.gov/about/nersc-staff/programming-environments-and-models/rahulkumar-gayatri/)
as a collaborator on this project. He has provided the LAMMPS/EXAALT Snap-C
app we have used for this project, as well as expertise in building and running
it on several systems.

## Systems

The systems we have chosen are:
- OLCF Summit (IBM Power9 + NVIDIA V100 GPUs)
- NERSC Perlmutter (HPE Shasta x86 + NVIDIA A100 GPUs)
- OLCF Crusher (HPE Shasta x86 + AMD MI250X GPUs)

In each of the system directories in this repo, we provide the Dockerfile we
built for the system, some directions to run the benchmark on the system, and
some job output files. 

## Images

| System            | Image location                       |
| ------------------| -------------------------------------|
| Summit            | docker.io/stephey/exaalt:summit      |
| Perlmutter        | docker.io/stephey/exaalt:perlmutter  |
| Crusher           | docker.io/stephey/exaalt:crusher     |


## Contact

Questions or comments? Please contact `lastephey@lbl.gov`.
