# EXAALT on Crusher

Directions and materials for running Rahul's EXAALT LAMMPS Snapc benchmark
on Crusher in a container.

## How to build from source

If you like, you can build this image from source using
the Containerfile in this repo.

If you want to upload your own image, I suggest getting a 
free [dockerhub account](https://hub.docker.com/) or 
a [quay.io account](https://quay.io/). Both of these
are image registries. 

You'll also need to use Docker desktop (or Podman desktop).
If you use Podman, you can just substitue the `docker`
command with `podman`. 

Here's an example build command. You can substitute in your
own registry and image information. 

```
docker build -t <your dockerhub username>/<image name>:<image tag> .
```

Once your image has been built, you can push it to your registry. 
Here's an example of pushing an image to Dockerhub:

```
docker login docker.io
#enter username
#enter password
docker push <your dockerhub username>/<image name>:<image tag>
```

You should now be able to see your image on dockerhub and pull it
to other systems via `docker pull`, `singularity pull`, etc.

## OR How to pull pre-built image

Alternatively, you can pull the previously
built image from dockerhub via

```
singularity pull docker://stephey/exaalt:crusher
```

This will save the image in the current directory as `exaalt_crusher.sif`

## How to run

There are two scripts- the first is the outer wrapper called `submit_crusher.sh`. This
is where you can set the problem size and the node size.

The second is the `run_crusher.slurm` script. Note that you'll also need to modify this
script to change the number of nodes in the job. Slurm on Crusher doesn't currently
permit submitting a job without specifying `N`, so we'll just set it manually.

Note you will also need to modify things like the singularity file name and the 
path to your benchmark directory. 

To run:

```
./submit_crusher.sh
```

## Notes

Since Singularity on Crusher is still missing full `--rocm` support, the OLCF devs were kind enough
to provide a wrapper script to enable ROCM. I have lightly modified this from their original. It is
located in `files/singularity-rocm-wrapper.sh`. This can be removed once `--rocm` support is in place,
although this will mean that additional variables like `CRAY_*` and `ROCM_*` may need to passed 
explicitly into Singularity.

## Resources

https://docs.olcf.ornl.gov/systems/crusher_quick_start_guide.html

https://code.ornl.gov/olcf/vector_addition

