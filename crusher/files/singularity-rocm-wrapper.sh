#!/bin/bash

OPTS=$@

SINGULARITYBIN="/usr/bin/singularity"
SINGULARITYOPTS="$($SINGULARITYBIN help | awk '/Available Commands/, /^$/' | awk '{print $1}' | tail -n+2 | head -n-1)"

for opt in $SINGULARITYOPTS; do
  if [[ " $OPTS " =~ " $opt " ]]; then
    SINGULARITYCMD="$opt"
    break
  fi
done

if [[ " exec run shell test " =~ " $SINGULARITYCMD " ]]; then
  export SINGULARITYENV_LD_LIBRARY_PATH="$CRAY_LD_LIBRARY_PATH:$CRAY_MPICH_DIR/lib-abi-mpich:$LD_LIBRARY_PATH:/opt/cray/pe/lib64"
  export SINGULARITY_CONTAINLIBS="/opt/cray/xpmem/default/lib64/libxpmem.so.0,/usr/lib64/libcxi.so.1,/usr/lib64/libjson-c.so.3,/usr/lib64/libdrm_amdgpu.so.1,/usr/lib64/libdrm.so.2,/lib64/libtinfo.so.6,/usr/lib64/libatomic.so.1,/usr/lib64/libquadmath.so.0,/usr/lib64/libcurl.so.4,/usr/lib64/libnghttp2.so.14,/usr/lib64/libssh.so.4,/usr/lib64/libpsl.so.5,/usr/lib64/libssl.so.1.1,/usr/lib64/libcrypto.so.1.1,/usr/lib64/libgssapi_krb5.so.2,/usr/lib64/libldap_r-2.4.so.2,/usr/lib64/liblber-2.4.so.2,/usr/lib64/libkrb5.so.3,/usr/lib64/libk5crypto.so.3,/usr/lib64/libkrb5support.so.0,/usr/lib64/libsasl2.so.3,/usr/lib64/libkeyutils.so.1"

  BIND="--bind ${PWD},/usr/share/libdrm,/var/spool/slurm,/opt/cray"

  if [ ! -z $ROCM_PATH ]; then
    BIND="${BIND},${ROCM_PATH}"
    OPTS="$(echo $OPTS | sed "s|\(\b\)$SINGULARITYCMD |\1$SINGULARITYCMD --rocm |")"
    export SINGULARITYENV_LD_LIBRARY_PATH="$ROCM_PATH/lib:$ROCM_PATH/lib64:$SINGULARITYENV_LD_LIBRARY_PATH"
  fi

  for var in $(env | grep -o -e "^PALS.*=" -e "^SLURM.*=" -e "^ROCR.*=" -e "^ROCM.*=" -e "^HIP.*=" -e "^MPICH.*=" -e "^CRAY_MPICH.*=" -e "^CRAY.*=" -e "^PMI.*=" | sed 's/\(.*\)=/\1/'); do
    eval export SINGULARITYENV_$var=\$${var}
  done
  # for OSU benchmarks
  export SINGULARITYENV_MV2_COMM_WORLD_LOCAL_RANK="$SLURM_LOCALID"

  OPTS="$(echo $OPTS | sed "s|\(\b\)$SINGULARITYCMD |\1$SINGULARITYCMD --cleanenv $BIND |")"
fi

$SINGULARITYBIN $OPTS
