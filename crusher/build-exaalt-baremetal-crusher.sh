#!/bin/bash

set -xe

module load craype-accel-amd-gfx90a
module load rocm/5.3.0
module load cmake

threads=64

HOME=$(pwd)
SRC=${HOME}/mainline_lammps
INSTALL_PREFIX="${HOME}/mainline_lammps_install"
export CRAYPE_LINK_TYPE=dynamic

export MPICH_GPU_SUPPORT_ENABLED=1

# Check if I am using my latest cmake
echo $(cmake --version)

# git clone lammps
if [ ! -d ${SRC} ]; then
  git clone https://github.com/lammps/lammps.git ${SRC}
  cd ${SRC}
  git pull
  cd ..
fi
cd ${SRC}

if [ ! -d build ]; then
    mkdir build
fi
cd build
rm -rf *

cmake \
-D CMAKE_BUILD_TYPE=Release \
-D CMAKE_Fortran_COMPILER=ftn \
-D CMAKE_C_COMPILER=$(which hipcc) \
-D CMAKE_CXX_COMPILER=$(which hipcc) \
-D CMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
-D LAMMPS_EXCEPTIONS=on \
-D BUILD_SHARED_LIBS=on \
-D PKG_KOKKOS=yes -DKokkos_ARCH_VEGA90A=ON -D Kokkos_ENABLE_HIP=yes \
-D PKG_MANYBODY=yes \
-D PKG_REPLICA=yes \
-D PKG_ML-SNAP=yes \
-D PKG_EXTRA-FIX=yes \
-D PKG_MPIIO=yes \
../cmake

make -j${threads}
make install -j${threads}

cd ${HOME}
rm -rf ${SRC}


